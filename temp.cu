#define _USE_MATH_DEFINES

#include <assert.h>
#include <cufft.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <algorithm>
#include <chrono>
#include <numeric>

#include <thrust/device_vector.h>
#include <thrust/execution_policy.h>
#include <thrust/sequence.h>
#include <thrust/sort.h>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#ifdef __INTELLISENSE__
#define CUDA_KERNEL(...)
#define __syncthreads()
#else
#define CUDA_KERNEL(...) <<< __VA_ARGS__ >>>
#endif

#define create_dev(TYPE, dev_var, len) \
  TYPE* dev_var;                       \
  cudaMalloc(&dev_var, (len) * sizeof(TYPE))
#define dev_to_host(TYPE, dev_var, host_var, len) \
  cudaMemcpy(host_var, dev_var, (len) * sizeof(TYPE), cudaMemcpyDeviceToHost)
#define host_to_dev(TYPE, host_var, dev_var, len) \
  cudaMemcpy(dev_var, host_var, (len) * sizeof(TYPE), cudaMemcpyHostToDevice)
#define create_dev_form_host(TYPE, dev_var, host_var, len) \
  create_dev(TYPE, dev_var, len);                          \
  host_to_dev(TYPE, host_var, dev_var, len)

#define SUMPARALLEL_CHUNKSIZE (2)
#define getMiliSeconds(t) \
  (std::chrono::duration_cast<std::chrono::microseconds>(t).count() / 1000.f)

// Tools: printDev
#define checkError()       \
  cudaDeviceSynchronize(); \
  printError(cudaGetLastError(), __LINE__);
inline void printError(const cudaError& status, const int& line,
                       bool abort = true) {
  if (status != cudaSuccess) {
    printf("\n[ERROR] in line: %i\t message: \"%s\"\n", line,
           cudaGetErrorString(status));
    if (abort) exit(0);
  }
}
#define show(var) std::cout << #var << " : " << var << std::endl;
#define showDev(arr, len) \
  printf("\n%s: ", #arr); \
  printDev(arr, len);
__global__ void printKernel(const float* dev_arr, const uint32_t len = 1) {
  if (threadIdx.x + blockDim.x * blockIdx.x == 0) {
    for (uint32_t i = 0; i < (uint32_t)min(10, len); ++i)
      printf("%g, ", dev_arr[i]);
    printf("\n");
    printf("\n");
  }
}
__global__ void printKernel(const cuComplex* dev_arr, const uint32_t len = 1) {
  if (threadIdx.x + blockDim.x * blockIdx.x == 0) {
    for (uint32_t i = 0; i < (uint32_t)min(10, len); ++i)
      printf("(%g, %g), ", dev_arr[i].x, dev_arr[i].y);
    printf("\n");
  }
}
__global__ void printKernel(const uint32_t* dev_arr, const uint32_t len = 1) {
  if (threadIdx.x + blockDim.x * blockIdx.x == 0) {
    for (uint32_t i = 0; i < (uint32_t)min(10, len); ++i)
      printf("%u, ", dev_arr[i]);
    printf("\n");
  }
}
template <typename T>
inline void printDev(const T* dev_arr, const uint32_t len) {
  cudaDeviceSynchronize();
  printKernel CUDA_KERNEL(1, 1)(dev_arr, len);
  cudaDeviceSynchronize();
}

// KERNEL: sum
__global__ void sumSerialKernel(float* out, const float input[],
                                const uint32_t len, float scale = 1.f) {
  if (0 == (threadIdx.x + blockDim.x * blockIdx.x)) {
    float sumVal = 0;
    for (uint32_t i = 0; i < len; ++i) {
      sumVal += input[i];
    }
    *out = sumVal / scale;
  }
}

__global__ void sumParallelKernel(float* sumValue, const float input[],
                                  const uint32_t len) {
  extern __shared__ float sdata[];  // size: 2 * blockDim.x
  // each thread loads one element from global to shared mem
  // note use of 1D thread indices (only) in this kernel

  const uint32_t thrd_i = threadIdx.x;
  for (uint32_t i = thrd_i + blockIdx.x * blockDim.x * 2;
       i < len + 2 * blockDim.x * gridDim.x; i += 2 * blockDim.x * gridDim.x) {
    sdata[thrd_i] = (i < len) ? input[i] : 0;
    sdata[thrd_i + blockDim.x] =
        (i + blockDim.x < len) ? input[i + blockDim.x] : 0;

    __syncthreads();

    // do reduction in shared mem
    for (uint32_t s = blockDim.x; s > 0; s >>= 1) {
      if (thrd_i < s) {
        sdata[thrd_i] += sdata[thrd_i + s];
      }
      __syncthreads();
    }

    if (thrd_i == 0) atomicAdd(sumValue, sdata[0]);
  }
}

void inline sumParallel(float* sumValue, const float input[],
                        const uint32_t len, const uint32_t numberBlocks,
                        const uint32_t numberThreads) {
  sumParallelKernel<<<numberBlocks, numberThreads,
                      numberThreads * 2 * sizeof(float)>>>(sumValue, input,
                                                           len);
  // sumParallelKernel << <1, 1024 >> > (sumValue, input, len);
}

float testSumParallel1(const float input[], uint32_t len) {
  create_dev_form_host(float, dev_input, input, len);
  create_dev(float, dev_sumVal, len);

  sumParallel(dev_sumVal, dev_input, len, 1, 16);
  float sumVal;
  dev_to_host(float, dev_sumVal, &sumVal, 1);
  return sumVal;
}

int main() {
  printf("Let' s start !\n ");
  // uint32_t size = 17;
  for (uint32_t size = 1; size < 1000; ++size) {
    std::vector<float> a(size);
    for (size_t i = 0; i < a.size(); ++i) a[i] = i;
    const float realSumVal = std::accumulate(a.begin(), a.end(), 0);
    const float sumVal = testSumParallel1(a.data(), a.size());

    if (realSumVal != sumVal) {
      printf("\n+++++++++++\n That's %sOk\n",
             (realSumVal == sumVal) ? "" : "NOT ");
      show(a.size());
      show(realSumVal);
      show(sumVal);
    }
  }
  printf("\nDONE :)\n");
  return 0;
}
